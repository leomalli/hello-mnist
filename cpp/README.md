# C++ Implementation

## Requirements

Having `Eigen` installed on your system, `make` and a version of `gcc` above 13.0 -- I like using the `std::ranges` library at every chance I get.


## Infos

The bests results I could acheive are obtained with the following network structure:

    784 -> 256 -> 10
    
Over 100 iterations, the statistics are as follows:

    - Mean accuracy: 94.16%
    - Maximal: 98.36%
    - Minimal: 75.55%
    
Due to hardware limitations I only ran these for 20 epochs and did not conduct much experiments with meta-parameters tweaking, I believe we could acheive much more consistant and high results going this way.


The rest of the results are obtained using a simplified structure -- 784 -> 32 -> 10.
Also note that I did not implement batching and just adjusted the gradient input by input.


Using the current `main.cpp` parameters, we get that:

    - Training on the MNIST-train dataset for 10 epochs leads to an approximate average of 86% accuracy on the MNIST-test dataset.
    - Training on the MNIST-train dataset for 50 epochs leads to an approximate average of 88% accuracy on the MNIST-test dataset.
    - Training on the MNIST-train dataset for 100 epochs doesn't lead to noticable improvement -- my testing also resulted in a mean of about 88% accuracy.
    

The above test were conducted by retraining the model for one-hundred times and recording the final accuracy at the end of the training.
For each regime, the intervals are:

    - For 10 epochs, from 74.91% to 96.01% accuracy.
    - For 50 epochs, from 72.42% to 96.33% accuracy.
    - For 100 epochs, from 75.70% to 96.24% accuracy.


Since I didn't implement an advanced optimizer and just tweak learning rate on the fly, it is not surprising to learn that the accuracy doesn't really improve after the initial phase, i.e. the first 10 to 20 epochs -- this doesn't mean that the network doesn't improve in general, we very often see a slow upward trend.
Note that even with this ridiculously simplified regime, I managed to achieve about 97% accuracy by toying with the size of the hidden layer and the meta-parameters.


# Samples

With an single hidden layer of size 256, we are able to acheive 98.02% accuracy on the testing dataset.
On the following graph, we see the evolution of the accuracy (blue) and the mean loss (in orange).
![Accuracy and Mean Loss](./img/accuracyAndLoss.png "Accuracy and Mean Loss")


Here are some sample pictures of what the network still couldn't predict at the end.


![MNIST-4](./img/4.png "MNIST-4")\
This should be classified as a 4 but here is the prediction's vector:

    3.4e-07 2.1e-08 1.7e-12 7.5e-05 0.3 2.587e-07 9.104e-07 0.06 0.55 0.06
Meaning the network predicted, with 55% certainty, that it was an 8.


The following is labeled by the network as a 7, with 62% certainty.
![MNIST-9](./img/9.png "MNIST-9")\
Here is the full prediction vector:

    6.5e-11 3.3e-05 1.2e-07 4.7e-5 0.26 1.7e-05 3.8e-09 0.62 2.9e-05 0.12


## TODO

- Add method to save the network and load it -- could be useful when testing between different languages.
- Add examples for images that are labeled wrongly by the network after training.
