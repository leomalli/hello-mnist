
#include <Eigen/Core>
#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>
#include <random>
#include <ranges>
#include <sstream>
#include <type_traits>
#include <vector>

#include "Network.hpp"

namespace rv = std::ranges::views;

#define UNUSED(X) (void)(X)

Eigen::VectorXf toHotEncoding(int label);
void parseLine(std::istringstream& ss, std::vector<Eigen::VectorXf>& labels,
               std::vector<Eigen::VectorXf>& data);
void fileToDatabase(const std::string& inputFile,
                    std::vector<Eigen::VectorXf>& labels,
                    std::vector<Eigen::VectorXf>& data);
template <typename T, typename A>
void shuffle(std::vector<T, A>& vec1, std::vector<T, A>& vec2);
template <typename T, typename A>
void logToFile(const std::string& fileName, const std::vector<T, A>& vec);
void logToFile(const std::string& fileName,
               const std::vector<std::tuple<Eigen::VectorXf, Eigen::VectorXf,
                                            Eigen::VectorXf>>& vec);

int main() {
  // File used for accuracy recording
  std::string accuracyFile{"./accuracies.log"};
  std::string lossFile{"./losses.log"};
  std::string missclassifiedFile{"./missclassifications.log"};

  // Create the network and set meta-params
  // std::vector<int> dims{784, 256, 10};
  std::vector<int> dims{784, 32, 10};
  Network net(dims);
  int n_epochs{100};

  // Learning rate related stuff
  net.setLearningRate(0.007f);
  float learningRateScaleFactor{0.9f};
  int learningRateChangeStep{4}, learningRateUpperLimitForChange{70};

  // Clip threshold definitions
  float clipThreshold{16.0f};
  net.setClipThreshold(clipThreshold);

  // Load and populate data
  std::vector<Eigen::VectorXf> testLabels, trainLabels, testData, trainData;

  // Loading datasets
  fileToDatabase("../mnist-data/mnist_test.csv", testLabels, testData);
  fileToDatabase("../mnist-data/mnist_train.csv", trainLabels, trainData);

  // Will store the evolution of accuracies on test dataset
  std::vector<float> accuracies, meanLosses;
  accuracies.reserve(n_epochs + 1);
  accuracies.push_back(net.testOn(testLabels, testData));

  // Logger function
#ifndef NDEBUG
  auto print_info = [&](size_t i, int iter) {
    Eigen::VectorXf pred{net.predict(trainData[i])};
    std::cout << "-----------------------------------\tIteration : " << iter
              << "---------------------------------\n";
    std::cout << "Chosen label is\t";
    for (auto x : trainLabels[i]) std::cout << x << ' ';
    std::cout << "\nIt is predicted as:\t";
    for (auto x : pred) std::cout << x << ' ';
    std::cout << "\nWich mean a cross entropy loss of:\t"
              << (-1.0f) * std::log(trainLabels[i].transpose() * pred);
    std::cout << "\n-----------------------------------------------------------"
                 "------------------------\n";
  };
#else
  auto print_info = [](size_t, int) {};
#endif

  std::cout << "-----------------------------------\tCURRENTLY "
               "TRAINING\t--------------------------------------\n";

  // Main learning loop
  for (auto i : rv::iota(0, n_epochs)) {
    shuffle(trainLabels, trainData);
    float loss = net.trainOn(trainLabels, trainData);
    meanLosses.push_back(loss);
    accuracies.push_back(net.testOn(testLabels, testData));
    print_info(0, i);
    if (i % learningRateChangeStep == 0 && i > 0 &&
        i < learningRateUpperLimitForChange) {
      net.scaleLearningRate(learningRateScaleFactor);
    }
#ifndef NDEBUG
    std::cout << "Learning rate:\t" << net.getLearningRate() << '\n';
#endif
  }

  // Log first and last accuracies and save the rest to a file
  std::cout << "--------------------------------------------RESULTS------------"
               "----------------------------------\n\n";
  std::cout << "Accuracy Before Training:\t\t" << 100.0f * accuracies.front()
            << "%\n";
  std::cout << "The max ever attained is:\t\t"
            << 100.0f * *std::max_element(accuracies.begin(), accuracies.end())
            << "%\n";
  std::cout << "\nFinal accuracy is:\t\t\t\t" << 100.0f * accuracies.back()
            << "%\n";

  logToFile(accuracyFile, accuracies);
  logToFile(lossFile, meanLosses);

  // Get some sample missclassification
  auto missclassifiedData =
      net.getMissclassifiedData(5, testLabels, testData);
  logToFile(missclassifiedFile, missclassifiedData);

  return 0;
}

Eigen::VectorXf toHotEncoding(int label) {
  Eigen::VectorXf res = Eigen::VectorXf::Zero(10);
  res[label] = 1.0f;
  return res;
}

void parseLine(std::istringstream& ss, std::vector<Eigen::VectorXf>& labels,
               std::vector<Eigen::VectorXf>& data) {
  // Store the results
  std::vector<int> vec;

  // parse the line
  std::string token;
  while (std::getline(ss, token, ',')) {
    vec.push_back(std::stoi(token));
  }
  // push the first entry as label
  labels.push_back(toHotEncoding(vec[0]));

  // parse the rest into a vector and push it to data
  Eigen::VectorXf img = Eigen::VectorXf::Zero(784);
  for (size_t i{0}; auto pixel : vec | rv::drop(1)) {
    img[i++] = static_cast<float>(pixel) / 256.0f;
  }
  data.push_back(img);
}

void fileToDatabase(const std::string& inputFile,
                    std::vector<Eigen::VectorXf>& labels,
                    std::vector<Eigen::VectorXf>& data) {
  // Starts by erasing the contents of labels and data
  labels.clear();
  data.clear();
  std::fstream file(inputFile, std::ios::in);
  std::string line;
  while (std::getline(file, line)) {
    auto ss = std::istringstream(line);
    parseLine(ss, labels, data);
  }
}

// Shuffles the two vectors accordingly
template <typename T, typename A>
void shuffle(std::vector<T, A>& vec1, std::vector<T, A>& vec2) {
  assert(vec1.size() == vec2.size());

  std::vector<int> indices(vec1.size());
  auto old_vec1{vec1}, old_vec2{vec2};
  std::iota(indices.begin(), indices.end(), 0);

  // Shuffle
  static std::random_device rd;
  static std::mt19937 rng{rd()};
  std::shuffle(indices.begin(), indices.end(), rng);

  std::transform(indices.begin(), indices.end(), vec1.begin(),
                 [&](std::size_t i) { return old_vec1[i]; });
  std::transform(indices.begin(), indices.end(), vec2.begin(),
                 [&](std::size_t i) { return old_vec2[i]; });
}

template <typename T, typename A>
void logToFile(const std::string& fileName, const std::vector<T, A>& vec) {
  std::ofstream outfile(fileName);
  outfile << std::setprecision(std::numeric_limits<float>::max_digits10);
  std::ostream_iterator<float> outputIterator(outfile, "\n");
  std::copy(vec.begin(), vec.end(), outputIterator);
}

void logToFile(const std::string& fileName,
               const std::vector<std::tuple<Eigen::VectorXf, Eigen::VectorXf,
                                            Eigen::VectorXf>>& vec) {
  std::ofstream outfile(fileName);
  outfile << std::setprecision(std::numeric_limits<float>::max_digits10);
  for (const auto&[label, data, wrongPrediction] : vec){
    outfile << "Label:\t" << label.transpose() << '\n';
    outfile << "Predicted as:\t" << wrongPrediction.transpose() << '\n';
    outfile << "Img:\t" << data.transpose() << '\n';
  }
}
