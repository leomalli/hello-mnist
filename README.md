# hello-mnist

## Goals

Main goal of the project is to familiarise myself with various programming languages.
For this purpose -- and for training purposes -- I will be implementing a really really barebone neural network.

The architecture of the network will be the same in all languages and will have as objective to get about 85% accuracy on [MNIST](https://en.wikipedia.org/wiki/MNIST_database) testing data -- I don't really want to make this project about the accuray of the network but more about programming it in different languages, hence I won't be really strict on myself with the attained accuracies.
The network will only consist of dense layer activated with ReLU and the general architecture will vary from project to project.


For more informations see `cpp/README.md` where I go a bit more in-depth with the analysis.


## Database

As I said before, I will be using MNIST as a database, and to simplify my life and avoid programming parsers, I will use a CSV version of it.
I found it on [Kaggle](https://www.kaggle.com/datasets/oddrationale/mnist-in-csv).

To be able to run my projects, just create a new dir at the root of the project named `mnist-data` and extract the MNIST-as-CSV inside.


## Status


Do not expect this project to be heavily maintained nor frequently updated.
As pointed out, I will use this only when a surge of curiosity gains me and pushes me to learn/explore new languages/frameworks.

Here are either the implementations already finished, or the implementations I am planning to do.

 - [X] C++ with `Eigen`
 - [ ] Python using only `numpy`
 - [ ] Python with `pytorch` -- This will have to wait until I have enough space on my disk to install `pytorch`...
 - [ ] Haskell... once I know what a Monad is
