#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import sys
from scipy.interpolate import make_interp_spline


# Load provided files and plot them
def main(files):
    # Load files
    accuracies = [np.loadtxt(f, dtype=float) for f in files]

    # Get max number of epochs
    max_epochs = np.max([len(a) for a in accuracies])
    for acc in accuracies:
        plot_accuracy(acc)

    plt.show()


def plot_accuracy(acc):
    X = np.linspace(0, len(acc)-1, len(acc))
    plt.plot(X, acc)

    # Plot a smoothed out version in dashed gray
    X_Y_Spline = make_interp_spline(X, acc)
    X_ = np.linspace(X.min(), X.max(), len(acc) // 10)
    Y_ = X_Y_Spline(X_)
    plt.plot(X_, Y_, c='gray', ls='--')


if __name__ == '__main__':
    files = sys.argv[1:]
    main(files)
