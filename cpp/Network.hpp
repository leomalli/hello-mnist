#ifndef NETWORK_H_
#define NETWORK_H_

#include <Eigen/Core>
#include <cassert>
#include <iostream>
#include <ranges>
#include <vector>

#include "Layer.hpp"
#define EPS_TOL 1E-4
#define MIN_LR 1E-6

namespace rv = std::ranges::views;

class Network {
 public:
  Network(const std::vector<int> &structure) {
    assert(structure.size() > 1);
    m_layers.reserve(structure.size() - 1);
    for (const auto &[inDim, outDim] : structure | rv::adjacent<2>)
      m_layers.emplace_back(inDim, outDim);

    m_layers.back().setAsOutput();
  }
  virtual ~Network() = default;

  Eigen::VectorXf predict(const Eigen::VectorXf &input) const {
    auto tmp{input};
    for (const auto &layer : m_layers) tmp = layer.predict(tmp);

    return tmp;
  }

  // Implement backpropagation given an input and the corresponding label
  float backPropagation(const Eigen::VectorXf &input,
                        const Eigen::VectorXf &label) {
    m_forwardPass(input);
    Eigen::VectorXf prediction = predict(input);
    Eigen::VectorXf dE{prediction - label};
    float loss{m_crossEntropyLoss(prediction, label)};
    // std::cout << "[dE]\t" << dE << '\n';

    // Backpropagation of delta error
    for (auto &layer : m_layers | rv::reverse) {
      dE = layer.computeDelta(dE);
    }
    // Update the params
    for (auto &layer : m_layers) {
      layer.updateParameters(m_learningRate, m_clipThreshold);
    }

    return loss;
  }

  void printStatus() const {
    std::cout << "Printing Network status:\n";
    for (const auto &layer : m_layers) {
      layer.printStatus();
    }
  }

  void setLearningRate(float lr) { m_learningRate = lr; }
  void setClipThreshold(float threshold) { m_clipThreshold = threshold; }

  // Returns the mean loss for this epoch
  float trainOn(std::vector<Eigen::VectorXf> &labels,
                std::vector<Eigen::VectorXf> &data) {
    float lossAcc{0.0f};
    for (const auto &[label, input] : rv::zip(labels, data)) {
      lossAcc += backPropagation(input, label);
    }
    return lossAcc / (float)data.size();
  }

  float testOn(std::vector<Eigen::VectorXf> &labels,
               std::vector<Eigen::VectorXf> &data) {
    size_t success{0}, total_size{data.size()};
    for (const auto &[label, input] : rv::zip(labels, data)) {
      Eigen::VectorXf prediction = predict(input);
      auto maxElem = std::max_element(prediction.begin(), prediction.end());
      auto maxElemIndex = std::distance(prediction.begin(), maxElem);
      if (label[maxElemIndex] == 1.0f) ++success;
    }
    return static_cast<double>(success) / static_cast<double>(total_size);
  }
  std::vector<std::tuple<Eigen::VectorXf, Eigen::VectorXf, Eigen::VectorXf>>
  getMissclassifiedData(size_t n_data, std::vector<Eigen::VectorXf> &labels,
                        std::vector<Eigen::VectorXf> &data) {
    std::vector<std::tuple<Eigen::VectorXf, Eigen::VectorXf, Eigen::VectorXf>> missclassified;
    // Test images
    for (const auto &[label, input] : rv::zip(labels, data)) {
      Eigen::VectorXf prediction = predict(input);
      auto maxElem = std::max_element(prediction.begin(), prediction.end());
      auto maxElemIndex = std::distance(prediction.begin(), maxElem);
      // When wrong add to vector
      if (label[maxElemIndex] != 1.0f) {
        missclassified.emplace_back(label, input, prediction);
      }
      if (missclassified.size() >= n_data) return missclassified;
    }

    return missclassified;
  }

  void scaleLearningRate(float r) {
    m_learningRate *= r;
    if (m_learningRate < MIN_LR) m_learningRate = MIN_LR;
  }
  void scaleClipThreshold(float r) { m_clipThreshold *= r; }

  float getLearningRate() const { return m_learningRate; }

 private:
  std::vector<Layer> m_layers;
  float m_learningRate{0.05f}, m_clipThreshold{10.0f};

  void m_forwardPass(const Eigen::VectorXf &input) {
    m_layers[0].forwardPropagate(input);
    for (size_t i{1}; i < m_layers.size(); ++i)
      m_layers[i].forwardPropagate(m_layers[i - 1].getActivated());
  }

  float m_crossEntropyLoss(const Eigen::VectorXf &input,
                           const Eigen::VectorXf &label) const {
    static auto clipAndLog = [](float x) -> float {
      return (x > EPS_TOL) ? std::log(x) : std::log(EPS_TOL);
    };

    return (-1.0f) * label.transpose() * input.unaryExpr(clipAndLog);
  }

  float m_dCrossEntropyLoss(const Eigen::VectorXf &prediction,
                            const Eigen::VectorXf &label) const {
    static auto clip = [](float x) -> float {
      return (x > EPS_TOL) ? x : EPS_TOL;
    };

    return (-1.0f) / (label.transpose() * prediction.unaryExpr(clip));
  }
};

#endif  // NETWORK_H_
