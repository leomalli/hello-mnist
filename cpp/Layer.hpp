#ifndef LAYER_H_
#define LAYER_H_

#include <Eigen/Core>
#include <cassert>
#include <cmath>
#include <functional>
#include <iostream>
#include <numeric>
#include <random>

class Layer {
 public:
  Layer(int nInputs, int nOutputs) {
    m_weights = Eigen::MatrixXf::Zero(nInputs, nOutputs)
                    .unaryExpr([&](auto) -> float {
                      return m_getRandom() * std::sqrt(2.0f / (float)nInputs);
                    })
                    .matrix();
    m_biases = Eigen::VectorXf::Zero(nOutputs);
  }
  Layer(const Layer &other) = default;
  ~Layer() = default;

  // Propagates input forward and save state
  void forwardPropagate(const Eigen::VectorXf &input) {
    m_inputs = input;
    m_outputs = m_weights.transpose() * m_inputs + m_biases;
  }

  Eigen::VectorXf getActivated() { return m_Activation(m_outputs); }

  // Set the activation as the softmax function and define the corresponding
  void setAsOutput() {
    m_Activation = [](const Eigen::VectorXf &state) -> Eigen::VectorXf {
      Eigen::VectorXf exponentials{state.array().exp().matrix()};
      return exponentials / exponentials.sum();
    };
  }

  Eigen::VectorXf predict(const Eigen::VectorXf &input) const {
    return m_Activation(m_weights.transpose() * input + m_biases);
  }

  Eigen::VectorXf getPrediction() const { return m_Activation(m_outputs); }

  Eigen::VectorXf getOutput() const { return m_outputs; }

  Eigen::VectorXf computeDelta(const Eigen::VectorXf &propagatedError) {
    // Coefficientwise multiplication
    m_delta = propagatedError.array() * m_dActivation(m_outputs).array();
    return m_weights * m_delta;
  }

  void updateParameters(float learningRate, float clipThreshold) {
    Eigen::MatrixXf gradBias{m_delta},
        gradWeights{m_inputs * m_delta.transpose()};

    m_clip(gradBias, clipThreshold);
    m_clip(gradWeights, clipThreshold);
    m_biases -= learningRate * gradBias;
    m_weights -= learningRate * gradWeights;

    // Clip the parameters...
    // m_clip(m_biases, 100.0f);
    // m_clip(m_weights, 100.0f);
  }

  void printStatus() const {
    std::cout << "Weights:\n" << m_weights << '\n';
    std::cout << "Biases:\n" << m_biases << '\n';
  }

 private:
  // Parameters
  Eigen::MatrixXf m_weights;
  Eigen::VectorXf m_biases;

  // Informations
  Eigen::VectorXf m_inputs, m_outputs;
  Eigen::MatrixXf m_delta;

  float m_getRandom() const {
    static std::mt19937 rng;
    static std::normal_distribution<> nd(0.0f, 1.0f);
    return nd(rng);
  }

  // Set activation a ReLU function
  std::function<Eigen::VectorXf(const Eigen::VectorXf &)> m_Activation =
      [](const Eigen::VectorXf &state) -> Eigen::VectorXf {
    return state.cwiseMax(0.0);
  };

  std::function<Eigen::VectorXf(const Eigen::VectorXf &)> m_dActivation =
      [](const Eigen::VectorXf &state) -> Eigen::VectorXf {
    return state.array().unaryExpr(
        [](auto &x) { return static_cast<float>(x > 0); });
  };

  // Ensures that the gradients stay in a reasonable range
  void m_clip(Eigen::VectorXf &vec, float clipVal) {
    assert(clipVal > 0);
    static auto clip = [clipVal](float x) -> float {
      return std::min(std::max(-clipVal, x), clipVal);
    };
    vec = vec.unaryExpr(clip);
  }
  void m_clip(Eigen::MatrixXf &mat, float clipVal) {
    assert(clipVal > 0);
    static auto clip = [clipVal](float x) -> float {
      return std::min(std::max(-clipVal, x), clipVal);
    };
    mat = mat.unaryExpr(clip);
  }
};

#endif  // LAYER_H_
